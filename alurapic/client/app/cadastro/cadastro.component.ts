import { Component, Input } from '@angular/core';
import { FotoComponent } from '../foto/foto.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FotoService } from '../foto/foto.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'cadastro',
    templateUrl: './cadastro.component.html' 
})
export class CadastroComponent { 

    foto: FotoComponent = new FotoComponent();
    meuForm: FormGroup;
    service: FotoService;

    // Recebe os parâmetros da rota
    route: ActivatedRoute;
    router: Router;

    mensagem: string = '';

    constructor(service: FotoService, fb: FormBuilder, route: ActivatedRoute, router: Router) {

        this.service = service;

        this.route = route;
        this.router = router;
        
        this.route.params.subscribe(params => {

            let id = params['id'];

            // Se o 'id' for preenchido será feita uma busca por ele
            if(id) {

                this.service
                .buscaPorId(id)
                .subscribe(foto => this.foto = foto, 
                           erro => console.log(erro)
                        );
            }

        });
        
        //Cria um grupo de validação com um objeto JSON
        //O nome dos atributos deve ser igual ao nome dado no 'formControlName' no documento HTML
        this.meuForm = fb.group({
            titulo: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            url: ['', Validators.required],
            descricao: ['']
        });
    }

    cadastrar(event) {
        event.preventDefault();
        console.log(this.foto);

        //Aqui virá o uso do service para realizar a requisção de cadastro
        this.service.cadastra(this.foto)
            .subscribe((res) => {
                this.mensagem = res.mensagem;
                //Limpa o formulário após a inserção
                this.foto = new FotoComponent();
                if(!res.isInclusao) {
                    this.router.navigate(['']);
                }
            }, error => console.log(error));
    }
}