import { Http, Headers, Response } from '@angular/http';
import { FotoComponent } from './foto.component';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

// Precisamos anotar FotoService com @Injectable, caso contrário Angular não entenderá que 
// deve procurar as dependências do próprio serviço quando for injetá-lo. Além disso, a própria classe precisa 
// ser adicionada como provider no módulo FotoModule para que a injeção funcione.
@Injectable()
export class FotoService {

    http: Http;
    headers: Headers;
    url: string = 'v1/fotos';

    constructor(http: Http) {

        this.http = http;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');

    }

    cadastra(foto: FotoComponent): Observable<MensagemCadastro> {

        // Se a foto possuir 'id' o método 'put' será chamado para salvar a foto no server
        if(foto._id) {

            // O parametro 'foto' é utilizado para fazer o envio da foto para o server
            return this.http
                .put(this.url + '/' + foto._id, JSON.stringify(foto), { headers: this.headers })
                .map(() => new MensagemCadastro('Foto alterada com sucesso!', false));

        } else {
            return this.http
                .post(this.url, JSON.stringify(foto), { headers: this.headers })
                .map(() => new MensagemCadastro('Foto incluida com sucesso', true));
        }
  
    }

    lista(): Observable<FotoComponent[]> {

        //Faz uma requisição 'get' ao servidor.
        //'stream' Para poder acessar as informações vindas do servidor, é necessário se inscrever no canal 
        //da resposta.
        return this.http.get(this.url)
            .map(res => res.json());

    }

    remove(foto: FotoComponent) {

        return this.http.delete(this.url + '/' + foto._id);
    }

    buscaPorId(id: string): Observable<FotoComponent> {

        return this.http
            .get(this.url + '/' + id)
            .map(res => res.json()
        );

    } 

}

export class MensagemCadastro {

    constructor(private _mensagem: string, private _inclusao: boolean) { }

    get mensagem(): string {

        return this._mensagem;

    }

    get isInclusao(): boolean {

        return this._inclusao;

    }

}