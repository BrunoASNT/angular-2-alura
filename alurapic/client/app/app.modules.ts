import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';

//Importando o modulo de fotos
import { FotoModule } from './foto/foto.module';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import { PainelModule } from './painel/painel.module';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ListagemComponent } from './listagem/listagem.component';
import { routing } from './app.routes';
//ReactiveFormModule - atraves dele que é possivel fazer as validacoes no modulo
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    //Determina que a aplicação rodará em um navegador
    imports: [ 
        BrowserModule, 
        FotoModule, 
        HttpModule, 
        PainelModule, 
        routing, 
        FormsModule, 
        ReactiveFormsModule ],
    //Declara o que faz parte do módulo
    declarations: [ AppComponent, CadastroComponent, ListagemComponent ],
    //Determina qual componente será iniciado
    bootstrap: [ AppComponent ]
})

//Transforma a classe em um módulo do Angular
export class AppModule {

}