import { Component, Input, OnInit, ElementRef } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'painel',
    templateUrl: './painel.component.html',
    styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit {
    
    @Input() titulo: string;

    // Encapsula o elemento nativo do DOM
    elemento: ElementRef;

    constructor(elemento: ElementRef) {

        this.elemento = elemento;
    }

    //Este método é chamado logo após o 'titulo' ter sido iniciado
    ngOnInit() {
        this.titulo = this.titulo.length > 7 ?
             this.titulo.substr(0, 7) + '...' : 
             this.titulo;
    }

    fadeOut(callback) {

        $(this.elemento.nativeElement).fadeOut(callback);
    }
    
}