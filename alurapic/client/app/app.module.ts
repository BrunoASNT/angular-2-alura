import 'rxjs/add/operator/map';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { ListagemComponent }   from './listagem/listagem.component';
import { CadastroComponent }   from './cadastro/cadastro.component';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

//Importando o modulo de fotos
import { FotoModule } from './foto/foto.module';
import { PainelModule } from './painel/painel.module';
import { routing } from './app.routes';
import { BotaoModule } from './botao/botao.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  //Determina que a aplicação rodará em um navegador
  imports:[ 
    BrowserModule, 
    HttpModule, 
    PainelModule, 
    FotoModule,
    routing,
    BotaoModule,
    FormsModule,
    ReactiveFormsModule
  ],
  //Declara o que faz parte do módulo
  declarations: [ AppComponent, ListagemComponent, CadastroComponent ],
  //Determina qual componente será iniciado
  bootstrap:    [ AppComponent ]
})
//Transforma a classe em um módulo do Angular
export class AppModule { }