import { RouterModule, Routes } from '@angular/router';
import { ListagemComponent } from './listagem/listagem.component';
import { CadastroComponent } from './cadastro/cadastro.component';

const appRoutes: Routes  = [

  //Configurando as rotas
  { path: '', component: ListagemComponent },
  { path: 'cadastro', component: CadastroComponent },
  // Usado em 'cadastro.component' e 'foto.service'
  { path: 'cadastro/:id', component: CadastroComponent },
  { path: '**', redirectTo: ''}
];

//Compila as rotas e são exportadas para 'app.modules.ts'
export const routing = RouterModule.forRoot(appRoutes);